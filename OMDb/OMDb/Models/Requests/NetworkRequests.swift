//
//  NetworkRequests.swift
//  OMDb
//
//  Created by Ramazan Ocak on 17.10.2020.
//  Copyright © 2020 Ramazan Ocak. All rights reserved.
//

import Foundation
import Alamofire

class NetworkRequests {
    func createUrl(with endpoint: String) -> String {
        return "http://www.omdbapi.com/?apikey=db5ae912"+endpoint
    }
    var count = 0
    static let shared = NetworkRequests()
    
    
    func request<T : Decodable>(progress:Bool = true,with link : String,method: HTTPMethod = .get,parameters: Parameters? = nil,encoding:ParameterEncoding = URLEncoding.default,success: @escaping (_ result: T? ) -> ()) {
        Alamofire.request(link, method: method, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON { response  in
                self.parseDate(response, success: success)
        }
    }
    
    
    
    func parseDate<T:Decodable>(_ response:DataResponse<Any>, success: @escaping (_ result: T?) -> ()) {
		if !InternetControllerManager.isConnectedToNetwork() {
			success(nil)
			AppUtils.shared.showAlert(title: "OOPS", message: "No Internet Connection")
			return
		}
        switch response.result {
        case .success:
            print("🍏 \(String(describing: link)), status code : \(response.response?.statusCode ?? 0)")
            do{
                let decoder = JSONDecoder()
                let result = try decoder.decode(T.self, from: response.data!)
                DispatchQueue.main.async {
                    success(result)
                }
            } catch {
                print("🍎 JSONDecoder : \(String(describing: link)) , ERROR : \(error.localizedDescription)")
                DispatchQueue.main.async {
                    success(nil)
                }
            }
        case .failure:
            if let data = response.data {
                if let json = String(data: data, encoding: String.Encoding.utf8) {
                    success(nil)
                    print("🍎 \(String(describing: link)), status code : \(response.response?.statusCode ?? 0)")
                    print("🍎 Failure Response: \(json)")
                }
                
            }
        }
    }
}
extension NetworkRequests {
    
    func movieDetailApiMethod(movieTitle: String ,onSuccess success: @escaping (_ result: MovieDetailResponse?) -> ()) {
        request(progress: true, with: createUrl(with: "&t=\(movieTitle)"), method: .get, parameters: nil, encoding: URLEncoding.default) { (response:MovieDetailResponse?) in
            success(response)
        }
    }
	
    func AllMovieApiMethod(movieTitle: String, page: Int ,onSuccess success: @escaping (_ result: SearchMovieResponse?) -> ()) {
        request(progress: true, with: createUrl(with: "&s=\(movieTitle)&page=\(page)"), method: .get, parameters: nil, encoding: URLEncoding.default) { (response:SearchMovieResponse?) in
            success(response)
        }
    }
	func movieDetailApiMethod(movieID: String ,onSuccess success: @escaping (_ result: MovieDetailResponse?) -> ()) {
        request(progress: true, with: createUrl(with: "&i=\(movieID)"), method: .get, parameters: nil, encoding: URLEncoding.default) { (response:MovieDetailResponse?) in
            success(response)
        }
    }
}
