//
//  SearchMovieResponse.swift
//  OMDb
//
//  Created by Ramazan Ocak on 17.10.2020.
//  Copyright © 2020 Ramazan Ocak. All rights reserved.
//

import Foundation

// MARK: - SearchMovie
struct SearchMovieResponse: Codable {
    let search: [Search]
    let totalResults, response: String

    enum CodingKeys: String, CodingKey {
        case search = "Search"
        case totalResults
        case response = "Response"
    }
}

// MARK: - Search
struct Search: Codable {
    let title, year, imdbID: String
    let type: TypeEnum
    let poster: String

    enum CodingKeys: String, CodingKey {
        case title = "Title"
        case year = "Year"
        case imdbID
        case type = "Type"
        case poster = "Poster"
    }
}

enum TypeEnum: String, Codable {
    case game = "game"
    case movie = "movie"
    case series = "series"
}
