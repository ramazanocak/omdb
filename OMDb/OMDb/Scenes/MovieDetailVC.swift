//
//  MovieDetailVC.swift
//  OMDb
//
//  Created by Ramazan Ocak on 17.10.2020.
//  Copyright © 2020 Ramazan Ocak. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class MovieDetailVC: UIViewController {

    @IBOutlet weak var movieImg: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelReleased: UILabel!
    @IBOutlet weak var labelRuntime: UILabel!
    @IBOutlet weak var labelGenre: UILabel!
    @IBOutlet weak var labelDirector: UILabel!
    @IBOutlet weak var labelDesc: UITextView!
	
	var movieDetail: Search!
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		NetworkRequests.shared.movieDetailApiMethod(movieID: movieDetail.imdbID) { (response) in
            
            if response == nil {
                self.navigationController?.popViewController(animated: true)
            }else {
                self.movieImg.sd_setImage(with: URL(string: response?.poster ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
                self.labelTitle.text = response?.title ?? ""
                self.labelGenre.text = response?.genre ?? ""
                self.labelRuntime.text = response?.runtime ?? ""
                self.labelDirector.text = response?.director ?? ""
                self.labelDesc.text = response?.plot ?? ""
                self.labelReleased.text = response?.released ?? ""
				Analytics.logEvent("MovieDetailLogs", parameters: ["FilmName" : response?.title ?? ""])
            }
        }
    }

}
