//
//  HomeVC.swift
//  OMDb
//
//  Created by Ramazan Ocak on 17.10.2020.
//  Copyright © 2020 Ramazan Ocak. All rights reserved.
//

import UIKit
import SDWebImage

class HomeVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UISearchBarDelegate {

	var searchArr = [Search]()
	var paginationCount = 1
    var lastSearchMovieText = ""
	
	@IBOutlet weak var searchCollectionView: UICollectionView!
	let searchVC = UISearchController(searchResultsController: nil)
	
	override func viewDidLoad() {
        super.viewDidLoad()
		
		let nib = UINib(nibName: "MovieCell", bundle: nil)
		searchCollectionView?.register(nib, forCellWithReuseIdentifier: "MovieCell")
		
		searchCollectionView.delegate = self
		searchCollectionView.dataSource = self
		//AppUtils.shared.showCustomLoading()
		DispatchQueue.main.asyncAfter(deadline: .now()+1) {
			AppUtils.shared.hideCustomLoading()
		}
        searchVC.hidesNavigationBarDuringPresentation = false
        searchVC.dimsBackgroundDuringPresentation = false
		searchVC.searchBar.delegate = self
		searchVC.searchBar.placeholder = "Film Ara"
		navigationItem.titleView = searchVC.searchBar
	    searchCollectionView.keyboardDismissMode = UIScrollView.KeyboardDismissMode.onDrag
        // Do any additional setup after loading the view.
    }

	func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
		
		var trimmedText = searchBar.text?.trimmingCharacters(in: .whitespaces)
			trimmedText = trimmedText?.removeWhitespace()
			if trimmedText?.count == 0 && trimmedText == "" {
				return
			}
			
			self.lastSearchMovieText = trimmedText ?? ""
			AllMovieApiMethod(with: 1, trimmedText: trimmedText)
			searchBar.text = ""

	}
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return searchArr.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieCell", for: indexPath) as! MovieCell
		cell.movieNameLabel.text = searchArr[indexPath.row].title
		cell.imgCell.sd_setImage(with: URL(string: searchArr[indexPath.row].poster), placeholderImage: UIImage(named: "placeholder.png"))
		cell.movieReleasedLabel.text = searchArr[indexPath.row].year
		return cell
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return CGSize(width: UIScreen.main.bounds.width, height: 280)
	}
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		
		let item = searchArr[indexPath.row]
		guard let vc = UIStoryboard(name: Constants().st_Main, bundle: nil).instantiateViewController(withIdentifier: Constants().stid_MovieDetailVC) as? MovieDetailVC else { return }
		vc.movieDetail = item
        self.navigationController?.pushViewController(vc, animated: true)
		
	}
	
	func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
            if indexPath.row == searchArr.count - 1 {  //numberofitem count
                self.paginationCount += 1
                AllMovieApiMethod(with: self.paginationCount, trimmedText: self.lastSearchMovieText)
            }
    }

}
extension HomeVC {
	func AllMovieApiMethod(with page: Int,trimmedText: String? = "")  {
        AppUtils.shared.showCustomLoading()
        if page == 1 {
            self.searchArr.removeAll()
        }
        DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                NetworkRequests.shared.AllMovieApiMethod(movieTitle: trimmedText!,page: page) { (response) in
                AppUtils.shared.hideCustomLoading()
                if response == nil {
					AppUtils.shared.showAlert(title: "OOPS", message: "There is no movie to show")
                }else {
                    if let response = response {
                        self.searchArr.append(contentsOf: response.search)
                    }
                    DispatchQueue.main.async {
                        self.searchCollectionView.reloadData()
                    }
                }
            }
        }
    }
}
