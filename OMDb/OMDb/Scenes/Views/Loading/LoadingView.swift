//
//  MovieDetailResponse.swift
//  OMDb
//
//  Created by Ramazan Ocak on 17.10.2020.
//  Copyright © 2020 Ramazan Ocak. All rights reserved.
//

import UIKit
import Lottie

class LoadingView: UIView {
    @IBOutlet weak var lottieView: AnimationView!

    override func awakeFromNib() {
        super.awakeFromNib()
 
        lottieView.isHidden = false
        lottieView.animation = Animation.named("loading")
        lottieView.loopMode = .loop
        lottieView.play()
	}
}
