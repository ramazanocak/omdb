//
//  MovieCell.swift
//  OMDb
//
//  Created by Ramazan Ocak on 17.10.2020.
//  Copyright © 2020 Ramazan Ocak. All rights reserved.
//

import UIKit

class MovieCell: UICollectionViewCell {
    @IBOutlet weak var imgCell: UIImageView!
    @IBOutlet weak var btnOnCell: UIButton!
    @IBOutlet weak var movieNameLabel: UILabel!
    @IBOutlet weak var movieReleasedLabel: UILabel!
}
