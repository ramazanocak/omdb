//
//  SplashVC.swift
//  OMDb
//
//  Created by Ramazan Ocak on 16.10.2020.
//  Copyright © 2020 Ramazan Ocak. All rights reserved.
//

import UIKit
import Firebase
import Lottie

class SplashVC: UIViewController {

	@IBOutlet weak var startAppButton: UIButton!
	
	override func viewDidLoad() {
        super.viewDidLoad()
		if !InternetControllerManager.isConnectedToNetwork() {
			AppUtils.shared.showAlert(title: "OOPS", message: "No Internet Connection")
			return
		}
		self.fetchRemoteConfig()
    }
	
	func updateViewRCValue() {
		let buttonText = RemoteConfig.remoteConfig().configValue(forKey: "buttonText").stringValue
		startAppButton.setTitle(buttonText, for: .normal)
	}
	
	func fetchRemoteConfig() {
		RemoteConfig.remoteConfig().fetchAndActivate { (status, error) in
			guard error == nil else{
				// Default bir Value belirlemissek onu kullanabiliriz.
				return
			}
			DispatchQueue.main.async {
				self.updateViewRCValue()
			}
		}
	}
}
extension SplashVC {
	@IBAction func goHomePage(_ sender: Any) {
		AppUtils.shared.showCustomLoading()
        DispatchQueue.main.asyncAfter(deadline: .now()+3) {
			if let navc =  AppUtils.shared.App_Delegate?.mainNavVC {
				AppUtils.shared.App_Delegate?.window?.rootViewController = navc
				AppUtils.shared.App_Delegate?.window?.makeKeyAndVisible()
			}
        }
	}
}
