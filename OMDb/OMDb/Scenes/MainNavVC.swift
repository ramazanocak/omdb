//
//  MainNavVC.swift
//  OMDb
//
//  Created by Ramazan Ocak on 18.10.2020.
//  Copyright © 2020 Ramazan Ocak. All rights reserved.
//

import UIKit
import Alamofire

class MainNavVC: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    // MARK: - offline online
    func startMonitoringInternetConnection(launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        startMonitoring()
    }
    
    let reachabilityManager = Alamofire.NetworkReachabilityManager()
    func startMonitoring() {
        reachabilityManager?.listener = { status in
            AppUtils.shared.App_Delegate?.hasInternet = status != NetworkReachabilityManager.NetworkReachabilityStatus.notReachable && status != NetworkReachabilityManager.NetworkReachabilityStatus.unknown
        }
        reachabilityManager?.startListening()
    }

}
