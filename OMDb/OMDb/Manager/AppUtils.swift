//
//  AppUtils.swift
//  OMDb
//
//  Created by Ramazan Ocak on 13.10.2020.
//  Copyright © 2020 Ramazan Ocak. All rights reserved.
//

import Foundation
import Alamofire
import CNPPopupController

class AppUtils: NSObject {
	static let shared = AppUtils()
	
	var loading: LoadingView?
	var loadingPopup: CNPPopupController?
	
	var App_Delegate: AppDelegate? {
		return UIApplication.shared.delegate as? AppDelegate
	}

	func showCustomLoading() {
        if loadingPopup == nil {
            loadingPopup?.dismiss(animated: false)
            
            loading = Bundle.main.loadNibNamed(Constants().nib_LoadingView, owner: self, options: nil)?.first as? LoadingView
            loading?.frame = UIScreen.main.bounds

            let theme = CNPPopupTheme.default()
            loadingPopup = CNPPopupController(contents: [loading!])

            if App_Delegate?.window?.rootViewController?.presentedViewController != nil {
                theme.backgroundColor = UIColor.black.withAlphaComponent(0.75)
			
            }
            else {
                theme.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            }
            
            theme.cornerRadius = 0.0
            theme.popupContentInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            theme.popupStyle = .centered
            theme.presentationStyle = .fadeIn
            theme.maskType = .clear
            theme.dismissesOppositeDirection = true
            theme.shouldDismissOnBackgroundTouch = false
            theme.movesAboveKeyboard = true
            theme.animationDuration = 0.25
            loadingPopup?.theme = theme
            loadingPopup?.present(animated: true)
            
        }
        else {
            if App_Delegate?.window?.rootViewController?.presentedViewController != nil {
                loadingPopup?.theme.backgroundColor = UIColor.black.withAlphaComponent(0.75)
            }
            else {
                loadingPopup?.theme.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            }
        }
    }
    func hideCustomLoading() {
        loadingPopup?.dismiss(animated: true)
        loadingPopup = nil
    }
	
	func showAlert(title: String, message: String) {
	
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okTitle = "OK"
        
        let okAction = UIAlertAction(title: okTitle, style: .cancel, handler: nil)
        alert.addAction(okAction)
        
        if let vc = AppUtils.shared.App_Delegate?.window?.rootViewController {
            if vc.presentedViewController != nil {
                vc.presentedViewController?.present(alert, animated: true, completion: nil)
            }
            else if vc.presentingViewController != nil {
                vc.presentingViewController?.present(alert, animated: true, completion: nil)
            }
            else {
                vc.present(alert, animated: true)
            }
        }
    }
}
extension String {
  func replace(string:String, replacement:String) -> String {
	  return self.replacingOccurrences(of: string, with: replacement, options: NSString.CompareOptions.literal, range: nil)
  }

  func removeWhitespace() -> String {
	  return self.replace(string: " ", replacement: "+")
  }
}

