//
//  Constants.swift
//  OMDb
//
//  Created by Ramazan Ocak on 16.10.2020.
//  Copyright © 2020 Ramazan Ocak. All rights reserved.
//

import UIKit

class Constants: NSObject {
	static let shared = Constants()
	
	let stid_SplashVC = "SplashVC"
	let stid_HomeVC = "HomeVC"
	let stid_MovieDetailVC = "MovieDetailVC"
	let st_Main = "Main"
	
	let nib_LoadingView = "LoadingView"
	let obs_noInternetConnection = "NoInternetConnection"

}

