//
//  AppDelegate.swift
//  OMDb
//
//  Created by Ramazan Ocak on 13.10.2020.
//  Copyright © 2020 Ramazan Ocak. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?
	var mainNavVC = UINavigationController(rootViewController: UIStoryboard(name: Constants().st_Main, bundle: nil).instantiateViewController(withIdentifier: Constants().stid_HomeVC))
	
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		
		FirebaseApp.configure()
	
		window = UIWindow(frame: UIScreen.main.bounds)
        if let splashvc = UIStoryboard(name: Constants().st_Main, bundle: nil).instantiateViewController(withIdentifier: Constants().stid_SplashVC) as? SplashVC {
            window?.rootViewController = splashvc
        }
        window?.makeKeyAndVisible()
		return true
	}

	
}

